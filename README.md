# pong
A pong game written in Jai.

## jai version
```console
$ jai -version
Version: beta 0.1.063, built on 12 May 2023.
```

## usage
```
$ jai ./build.jai
...
$ ./bin/game
```
